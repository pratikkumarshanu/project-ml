import numpy as np

def PCA(X, num_component):
    mu = np.mean(X, axis=0)
    Z = X - mu
    C = np.cov(Z, rowvar=False)
    [l,V] = np.linalg.eigh(C)
    l = np.flipud(l)
    V = np.flipud(V.T)
    P = np.dot(Z,V.T)
    return (mu, Z, C, V, P, P[:,0:num_component])

def reconstruct(X, V, num_component):
    mu, Z, C, V, P, P_c = PCA(X, num_component)
    R = np.dot(P_c, V[0:num_component,:])
    return R

def project_on_eigenvectors(Z, V, num_component):
    return np.dot(Z, V.T)[:,0:num_component]

def lambda_contributions(lambdas):
    l = []
    for i,x in enumerate(np.true_divide(np.cumsum(lambdas), sum(lambdas))):
        l.append((i+1,x))
    return l