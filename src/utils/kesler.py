import numpy as np

def keslerize(labels, total_class=10):
    kesler = np.empty((labels.shape[0],total_class))
    kesler.fill(-1)
    kesler[np.arange(labels.shape[0]), labels.flatten()] = 1
    return kesler.astype(int)

def dekeslerize(kesler):
    return np.array([[x] for x in kesler.argmax(axis=1)])