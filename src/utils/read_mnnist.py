#!/usr/bin/env python
import os
import struct
from array import array as pyarray

import numpy as np

from deskew import deskew_image


def load_mnist(dataset="training", digits=range(10), path=None):
    """
    Adapted from: http://cvxopt.org/applications/svm/index.html?highlight=mnist
    """
    if not path:
        path = os.getcwd()
    if dataset == "training":
        fname_img = os.path.join(path, 'train-images-idx3-ubyte')
        fname_lbl = os.path.join(path, 'train-labels-idx1-ubyte')
    elif dataset == "testing":
        fname_img = os.path.join(path, 't10k-images-idx3-ubyte')
        fname_lbl = os.path.join(path, 't10k-labels-idx1-ubyte')
    else:
        raise ValueError("dataset must be 'testing' or 'training'")

    flbl = open(fname_lbl, 'rb')
    magic_nr, size = struct.unpack(">II", flbl.read(8))
    lbl = pyarray("b", flbl.read())
    flbl.close()

    fimg = open(fname_img, 'rb')
    magic_nr, size, rows, cols = struct.unpack(">IIII", fimg.read(16))
    img = pyarray("B", fimg.read())
    fimg.close()

    ind = [ k for k in range(size) if lbl[k] in digits ]
    N = len(ind)

    images = np.zeros((N, rows, cols), dtype=np.uint8)
    labels = np.zeros((N, 1), dtype=np.int8)
    for i in range(len(ind)):
        images[i] = np.array(img[ ind[i]*rows*cols : (ind[i]+1)*rows*cols ]).reshape((rows, cols))
        labels[i] = lbl[ind[i]]

    return images, labels

def convertImagetoArray(images, normalize=False, deskew=False):
    # converting from NX28X28 array into NX784 array
    # normalize: normalizes the pixel value from 0-255 to 0-1
    # deskew: rotates the images so that the number appears straight
    flatimages = list()
    for i in images:
        img = i
        if normalize:
            img = img/255.0
        if deskew:
            img = deskew_image(img)
        flatimages.append(img.ravel())
    X = np.asarray(flatimages)
    return X