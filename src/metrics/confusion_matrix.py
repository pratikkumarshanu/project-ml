from sklearn.metrics import confusion_matrix

def get_confusion_matrix(actual, predicted, labels):
    return confusion_matrix([x for x in actual.flatten()], [x for x in predicted.flatten()], labels)