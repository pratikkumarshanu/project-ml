from sklearn.metrics import accuracy_score

def get_accuracy(actual, predicted):
    return accuracy_score([x for x in actual.flatten()], [x for x in predicted.flatten()])