from optparse import OptionParser
from utils.read_mnnist import load_mnist, convertImagetoArray
from utils.pca import PCA, project_on_eigenvectors
from classifiers.linear import LinearClassifier
from classifiers.neural import NeuralNetworkClassifier
from classifiers.em import ExpectationMaximizationClassifier


classifiers = ['linear', 'neural', 'em']

def validate(options):
    if options.classifier not in classifiers:
        print 'Classifier', options.classifier, 'not supported. Valid classifiers:', classifiers, 'Exiting!'
        exit(1)

    if options.pca:
        options.pca = int(options.pca)
        if (options.pca > 784 or options.pca < 0):
            print 'PCA components can be between 0 and 784 only. Exiting!'
            exit(1)
        else:
            print 'Selecting', options.pca, 'principal components from the features'

    if options.normalize:
        print 'Pixel value will be normalized from [0-255] to [0-1]'

    if options.deskew:
        print 'Images will be deskewed/rotated so that it appears straight'

parser = OptionParser()
parser.add_option("-c", "--classifier", dest="classifier", help="selects the classifier")
parser.add_option("-n", "--normalize", action="store_true", dest="normalize",
                  default=False, help="normalizes the pixel value from 0-255 to 0-1")
parser.add_option("-d", "--deskew", action="store_true", dest="deskew", default=False,
                  help="rotates the images so that the number appears straight")
parser.add_option("-p", "--pca", dest="pca", help="number of components[0-784] if you want to do pca")
(options, args) = parser.parse_args()
validate(options)

# read training utils
images, labels = load_mnist('training', path='../data/mnist')
X = convertImagetoArray(images, normalize=options.normalize, deskew=options.deskew)

# read testing utils
images_test, labels_test = load_mnist('testing', path='../data/mnist')
X_test = convertImagetoArray(images_test, normalize=options.normalize, deskew=options.deskew)

# PCA if needed
if options.pca:
    (_,_,_,V,_,P) = PCA(X, options.pca)
    X = P
    (_,Z,_,_,_,_) = PCA(X_test, options.pca)
    X_test = project_on_eigenvectors(Z, V, options.pca)

if options.classifier == 'linear':
    lc = LinearClassifier()
    lc.fit(X,labels)
    lc.predict(X_test,labels_test)
    print 'Accuracy:\n', lc.accuracy()
    print 'Confusion Matrix:\n', lc.confusion_matrix()

if options.classifier == 'neural':
    nnc = NeuralNetworkClassifier()
    nnc.fit(X, labels)
    nnc.predict(X_test, labels_test)
    print 'Accuracy:\n', nnc.accuracy()
    print 'Confusion Matrix:\n', nnc.confusion_matrix()

if options.classifier == 'em':
    emc = ExpectationMaximizationClassifier()
    emc.fit(X, labels)
    emc.predict(X_test, labels_test)
    print 'Accuracy:\n', emc.accuracy()
    print 'Confusion Matrix:\n', emc.confusion_matrix()

