import numpy as np
import random
from utils.kesler import keslerize, dekeslerize
from metrics.accuracy import get_accuracy
from metrics.confusion_matrix import confusion_matrix
class NeuralNetworkClassifier(object):

    def __init__(self):
        self.classifier = None

    def fit(self, X, T):
        p = Perceptron(X.shape[1])
        print p
        return

    def predict(self, X, T):
        return

    def accuracy(self):
        return

    def confusion_matrix(self):
        return

class Perceptron(object):
    def __init__(self, n_inputs):
        self.n_inputs = n_inputs
        self.weights =  [random.uniform(0,1) for i in xrange(0, n_inputs+1)]

    def set_weights(self, weights):
        self.weights = weights

    def output(self, inputs):
        print 1

    def __str__(self):
        return 'Weights: %s, Bias: %s' % ( str(self.weights[:-1]),str(self.weights[-1]) )


