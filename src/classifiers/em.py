import numpy as np
import random
from utils.kesler import keslerize, dekeslerize
from metrics.accuracy import get_accuracy
from metrics.confusion_matrix import confusion_matrix
class ExpectationMaximizationClassifier(object):

    def __init__(self):
        self.classifier = None

    def fit(self, X, T):
        return

    def predict(self, X, T):
        return

    def accuracy(self):
        return

    def confusion_matrix(self):
        return