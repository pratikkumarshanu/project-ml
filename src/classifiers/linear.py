import numpy as np
from utils.kesler import keslerize, dekeslerize
from metrics.accuracy import get_accuracy
from metrics.confusion_matrix import confusion_matrix
class LinearClassifier(object):

    def __init__(self):
        self.classifier = None
        self.train_X = None
        self.train_T = None
        self.test_X = None
        self.test_T = None
        self.test_predicted_T = None

    def fit(self, X, T):
        self.train_X = X
        self.train_T = T
        Xlinear = np.append(np.ones((X.shape[0],1), dtype=np.int), X, axis=1)
        Xinv = np.linalg.pinv(Xlinear)
        T = keslerize(T, 10)
        W = np.dot(Xinv, T)
        self.classifier = W

    def predict(self, X, T):
        self.test_X = X
        self.test_T = T
        Xlinear = np.append(np.ones((X.shape[0],1), dtype=np.int), X, axis=1)
        T_predicted = np.dot(Xlinear, self.classifier)
        T_class = dekeslerize(T_predicted)
        self.test_predicted_T = T_class
        return T_class

    def accuracy(self):
        if self.test_T is not None and self.test_predicted_T is not None:
            return get_accuracy(self.test_T, self.test_predicted_T)
        else:
            return -1

    def confusion_matrix(self):
        if self.test_T is not None and self.test_predicted_T is not None:
            return confusion_matrix(self.test_T, self.test_predicted_T, labels=[0,1,2,3,4,5,6,7,8,9])
        else:
            return -1
