# Running the classifier #

## Run the command from src directory ##
cd src

## Without pca and without deskewing ##
python classifier.py -c linear -n

## Without pca and with deskewing ##
python classifier.py -c linear -n -d

## With pca and without deskewing ##
python classifier.py -c linear -n -p 50

## With pca and with deskewing ##
python classifier.py -c linear -n -d -p 50